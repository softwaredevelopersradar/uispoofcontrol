﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using UISpoof.Classes;

namespace UISpoof
{
    public partial class SpoofingControl : UserControl, INotifyPropertyChanged
    {

        private PlotModel _plotModelCircle = new PlotModel();
        public PlotModel PlotModelCircle
        {
            get
            {
                return _plotModelCircle;
            }

            set
            {
                _plotModelCircle = value;
                OnPropertyChanged();
            }
        }

        private float _minSpeed = 0;
        public float MinSpeed
        {
            get
            {
                return _minSpeed;
            }

            set
            {
                _minSpeed = value;
                OnPropertyChanged();
            }
        }

        private float _maxSpeed = 300;
        public float MaxSpeed
        {
            get
            {
                return _maxSpeed;
            }

            set
            {
                _maxSpeed = value;
                OnPropertyChanged();
            }
        }



        private short _minElevation = 0;
        public short MinElevation
        {
            get
            {
                return _minElevation;
            }

            set
            {
                _minElevation = value;
                OnPropertyChanged();
            }
        }

        private short _maxElevation = 1000;
        public short MaxElevation
        {
            get
            {
                return _maxElevation;
            }

            set
            {
                _maxElevation = value;
                OnPropertyChanged();
            }
        }


        private short _minAngle = 0;
        public short MinAngle
        {
            get
            {
                return _minAngle;
            }

            set
            {
                _minAngle = value;
                OnPropertyChanged();
            }
        }

        

        private short _maxAngle = 360;
        public short MaxAngle
        {
            get
            {
                return _maxAngle;
            }

            set
            {
                _maxAngle = value;
                OnPropertyChanged();
            }
        }


        private string _regime = "";
        public string Regime
        {
            get
            {
                return _regime;
            }

            set
            {
                _regime = value;
                OnPropertyChanged();
            }
        }


        private DoubleCollection _tickMarks = new DoubleCollection() { 1,2,5,10,20,30,60};

        public DoubleCollection TickMarks
        {
            get
            {
                return _tickMarks;
            }

            set
            {
                _tickMarks = value;
                OnPropertyChanged();
            }
        }
        


        private DateTime _timeStart = new DateTime();

        private TimeSpan _timeWork = new TimeSpan();

        public TimeSpan TimeWork 
        {
            get
            {
                return _timeWork;
            }

            set
            {
                _timeWork = value;
                OnPropertyChanged();
            }
        }




        private ObservableCollection<DataPoint> _points { get; set; } = new ObservableCollection<DataPoint>();

        public ObservableCollection<DataPoint> Points
        {
            get
            {
                return _points;
            }

            set
            {
                _points = value;
                OnPropertyChanged();
            }
        }


        private ObservableCollection<DataPoint> _pointsDirection { get; set; } = new ObservableCollection<DataPoint>();

        public ObservableCollection<DataPoint> PointsDirection
        {
            get
            {
                return _points;
            }

            set
            {
                _points = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<DataPoint> _pointLast { get; set; } = new ObservableCollection<DataPoint>();

        public ObservableCollection<DataPoint> PointLast
        {
            get
            {
                return _pointLast;
            }

            set
            {
                _pointLast = value;
                OnPropertyChanged();
            }
        }





        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        #endregion
       
    }
}
