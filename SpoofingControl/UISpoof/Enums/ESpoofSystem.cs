﻿public enum ESpoofSystem : byte
{
    NONE = 0,
    GPS = 1,
    GLONASS, 
    GPS_GLONASS, 
    GPS_GLONASS_CH2
}