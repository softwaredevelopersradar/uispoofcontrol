﻿public enum EStatus:byte
{
    Stop = 0,
    Synchronization = 1,
    Generation = 2
}