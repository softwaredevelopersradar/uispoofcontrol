﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Resources;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace UISpoof
{
    

    public partial class SpoofingControl : UserControl
    {
        public  string GetResourceTitle (string key)
        {
            try
            {                
                return (string)this.Resources[key];
            }

            catch
            {
                return "";
            }
            
        }

        
        public void SetResourceLanguage(ELanguages language)
        {

            this.Resources.MergedDictionaries.Clear();
         
            ResourceDictionary dictionaryLanguages = new ResourceDictionary();
            ResourceDictionary dictionaryTheme = new ResourceDictionary();

            dictionaryTheme.Source = new Uri("/UISpoof;component/Themes/ThemeBlue.xaml",
                                      UriKind.Relative);  


            try
            {
                switch (language)
                {
                    case ELanguages.EN:
                        dictionaryLanguages.Source = new Uri("/UISpoof;component/Languages/StringResource.EN.xaml",
                                      UriKind.Relative);
                        
                        break;

                    case ELanguages.RU:
                        dictionaryLanguages.Source = new Uri("/UISpoof;component/Languages/StringResource.RU.xaml",
                                           UriKind.Relative);
                       
                        break;

                    case ELanguages.SB:
                        dictionaryLanguages.Source = new Uri("/UISpoof;component/Languages/StringResource.SB.xaml",
                            UriKind.Relative);

                        break;

                    default:
                        dictionaryLanguages.Source = new Uri("/UISpoof;component/Languages/StringResource.EN.xaml",
                                      UriKind.Relative);
                        
                        break;
                }

                

                this.Resources.MergedDictionaries.Add(dictionaryLanguages);
                this.Resources.MergedDictionaries.Add(dictionaryTheme);

            }
            catch 
            { }
        }

        

        private void basicProperties_OnLanguageChanged(object sender, ELanguages language)
        {            
            SetResourceLanguage(language);
            
        }


    }
}
