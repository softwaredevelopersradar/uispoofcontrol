﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
//using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UISpoof.Classes;

namespace UISpoof
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class SpoofingControl : UserControl
    {
        private Timer _timerWork = new System.Timers.Timer();

        public event EventHandler<SimulationParam> OnChangeInputParam;



        

        public SpoofingControl()
        {
            InitializeComponent();
            TempLanguage = new ELanguages();
            TempLanguage = ELanguages.EN;
            //SetResourceLanguage(ELanguages.RU);

            this.DataContext = this;
           
            InitializeTimerWork();
            
        }


        public static void circle(double x, double y, int width, int height, Panel cv)
        {
            Ellipse circle = new Ellipse()
            {
                Width = width,
                Height = height,
                Stroke = Brushes.Red,
                StrokeThickness = 6,
                Margin = new Thickness(x, y, 0, 0)
            };
            cv.Children.Add(circle);
        }

      

        private void InputParam_OnChangeParam(object sender, EventArgs e)
        {
            
        }

       

        private void PreviewTextInputHandler(object sender, TextCompositionEventArgs e)
        {
            // MIN Value is 0 and MAX value is 99

            var textBox = sender as TextBox;
            bool bFlag = false;
            if (!string.IsNullOrWhiteSpace(e.Text) && !string.IsNullOrWhiteSpace(textBox.Text))
            {
                string str = textBox.Text + e.Text;
                bFlag = str.Length <= 2 ? false : true;

                
                try
                {
                    byte b = Convert.ToByte(e.Text);
                }
                catch
                {

                }
            }
            e.Handled = (Regex.IsMatch(e.Text, "[^0-9]+") || bFlag);
        }


        private void InitializeTimerWork()
        {
            _timerWork.Elapsed += TickTimerWork;

            _timerWork.AutoReset = true;
        }

        void TickTimerWork(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                TimeWork = DateTime.Now.Subtract(_timeStart);
            }
            catch { }

        }


        private void StartTimeWork()
        {
            _timeStart = DateTime.Now;

            _timerWork.Enabled = true;
        }

        private void StopTimeWork()
        {
            _timerWork.Enabled = false;
        }

        private void SetStopParam()
        {
            OutputParam = new SimulationParam();         
            TimeWork = DateTime.Now.Subtract(DateTime.Now);
            Points.Clear();
            PointLast.Clear();
        }
            

        private void Slider_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            OnChangeInputParam?.Invoke(this, InputParam);

          

            //PointsDirection.Clear();
            //PointsDirection.Add(new OxyPlot.DataPoint((double)0,
            //                                                  (double)InputParam.Angle)); ;
            //PointsDirection.Add(new OxyPlot.DataPoint((double)10,
            //                                                  (double)InputParam.Angle));

        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox textBox = sender as TextBox;

           
        }

        
    }
}
