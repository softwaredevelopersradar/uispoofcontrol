﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using UISpoof.Classes;

namespace UISpoof
{
    public partial class SpoofingControl : UserControl
    {

        #region OutputParam
        public static readonly DependencyProperty OutputParamProperty = DependencyProperty.Register("OutputParam", typeof(SimulationParam),
                                                                        typeof(SpoofingControl), new PropertyMetadata(new PropertyChangedCallback(OutputParamChanged)));
         

        public SimulationParam OutputParam
        {
            get { return (SimulationParam)GetValue(OutputParamProperty); }
            set { SetValue(OutputParamProperty, value); }
        }


        private static void OutputParamChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SpoofingControl spoofControl = (SpoofingControl)d;

            try
            {
                if (spoofControl.OutputParam.Coordinate.Latitude > 0 &&
                    spoofControl.OutputParam.Coordinate.Longitude > 0)

                {
                    spoofControl.Points.Add(new OxyPlot.DataPoint((double)spoofControl.OutputParam.Coordinate.Longitude,
                                                              (double)spoofControl.OutputParam.Coordinate.Latitude));

                    if (spoofControl.PointLast.Count > 0)
                        spoofControl.PointLast.Remove(spoofControl.PointLast.Last());

                    spoofControl.PointLast.Add(new OxyPlot.DataPoint((double)spoofControl.OutputParam.Coordinate.Longitude,
                                                                  (double)spoofControl.OutputParam.Coordinate.Latitude));

                }

               

            }
            catch
            { }
        }

        #endregion


        #region InputParam
        public static readonly DependencyProperty InputParamProperty = DependencyProperty.Register("InputParam", typeof(SimulationParam),
                                                                        typeof(SpoofingControl), new PropertyMetadata(new SimulationParam(), new PropertyChangedCallback(InputParamChanged)));

        public SimulationParam InputParam
        {
            get { return (SimulationParam)GetValue(InputParamProperty); }
            set { SetValue(InputParamProperty, value); }
        }

        private static void InputParamChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SpoofingControl spoofControl = (SpoofingControl)d;

            try
            {
                //spoofControl.PointsDirection.Clear();
                //spoofControl.PointsDirection.Add(new OxyPlot.DataPoint((double)0,
                //                                                  (double)spoofControl.InputParam.Angle)); ;
                //spoofControl.PointsDirection.Add(new OxyPlot.DataPoint((double)10,
                //                                                  (double)spoofControl.InputParam.Angle));



            }
            catch
            { }
        }

        #endregion




        #region Status
        public static readonly DependencyProperty StatusProperty = DependencyProperty.Register("Status", typeof(EStatus),
                                                                        typeof(SpoofingControl), new PropertyMetadata(new PropertyChangedCallback(StatusChanged)));

        public EStatus Status
        {
            get { return (EStatus)GetValue(StatusProperty); }
            set { SetValue(StatusProperty, value); }
        }


        private static void StatusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SpoofingControl spoofControl = (SpoofingControl)d;
            spoofControl.Regime = spoofControl.GetResourceTitle((string)Enum.GetName(typeof(EStatus), spoofControl.Status));
 
            try
            {
                switch (spoofControl.Status)
                {
                    case EStatus.Stop:
                        spoofControl.StopTimeWork();
                        spoofControl.SetStopParam();
                        break;

                    case EStatus.Synchronization:
                        break;

                    case EStatus.Generation:
                        spoofControl.StartTimeWork();
                        break;

                    default:
                        break;
                }
            }
            catch
            { }
        }

        #endregion

        #region SatelitesGPS
        public static readonly DependencyProperty SatelitesGPSProperty = DependencyProperty.Register("SatelitesGPS", typeof(string),
                                                                        typeof(SpoofingControl), new PropertyMetadata(""));

        public string SatelitesGPS
        {
            get { return (string)GetValue(SatelitesGPSProperty); }
            set { SetValue(SatelitesGPSProperty, value); }
        }

        #endregion


        #region SatelitesGLONASS
        public static readonly DependencyProperty SatelitesGLONASSProperty = DependencyProperty.Register("SatelitesGLONASS", typeof(string),
                                                                        typeof(SpoofingControl), new PropertyMetadata(""));

        public string SatelitesGLONASS
        {
            get { return (string)GetValue(SatelitesGLONASSProperty); }
            set { SetValue(SatelitesGLONASSProperty, value); }
        }

        #endregion

        #region SatelitesBeidou
        public static readonly DependencyProperty SatellitesBeidouProperty = DependencyProperty.Register("SatellitesBeidou", typeof(string),
            typeof(SpoofingControl), new PropertyMetadata(""));

        public string SatellitesBeidou
        {
            get { return (string)GetValue(SatellitesBeidouProperty); }
            set { SetValue(SatellitesBeidouProperty, value); }
        }

        #endregion

        #region SatelitesGalileo
        public static readonly DependencyProperty SatellitesGalileoProperty = DependencyProperty.Register("SatellitesGalileo", typeof(string),
            typeof(SpoofingControl), new PropertyMetadata(""));

        public string SatellitesGalileo
        {
            get { return (string)GetValue(SatellitesGalileoProperty); }
            set { SetValue(SatellitesGalileoProperty, value); }
        }

        #endregion


        #region TwoSystemSpoofing
        public static readonly DependencyProperty TwoSystemSpoofingProperty = DependencyProperty.Register("TwoSystemSpoofing", typeof(bool),
                                                                        typeof(SpoofingControl), new PropertyMetadata(true));

        public bool TwoSystemSpoofing
        {
            get { return (bool)GetValue(TwoSystemSpoofingProperty); }
            set { SetValue(TwoSystemSpoofingProperty, value); }
        }

        #endregion


        #region SpoofGPS
        public static readonly DependencyProperty SpoofGPSProperty = DependencyProperty.Register("SpoofGPS", typeof(bool),
                                                                        typeof(SpoofingControl), new PropertyMetadata(true));

        public bool SpoofGPS
        {
            get { return (bool)GetValue(SpoofGPSProperty); }
            set { SetValue(SpoofGPSProperty, value); }
        }

        #endregion


        #region SpoofGLONASS
        public static readonly DependencyProperty SpoofGLONASSProperty = DependencyProperty.Register("SpoofGLONASS", typeof(bool),
                                                                        typeof(SpoofingControl), new PropertyMetadata(false));

        public bool SpoofGLONASS
        {
            get { return (bool)GetValue(SpoofGLONASSProperty); }
            set { SetValue(SpoofGLONASSProperty, value); }
        }

        #endregion


        #region TempLanguage

        public static readonly DependencyProperty TempLanguageProperty = DependencyProperty.Register("TempLanguage", typeof(ELanguages), typeof(SpoofingControl),
                                                                       new PropertyMetadata(new PropertyChangedCallback(TempLanguageChanged)));



        public ELanguages TempLanguage
        {
            get { return (ELanguages)GetValue(TempLanguageProperty); }
            set { SetValue(TempLanguageProperty, value); }
        }

        private static void TempLanguageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SpoofingControl spoofingControl = (SpoofingControl)d;

            try
            {
                spoofingControl.SetResourceLanguage(spoofingControl.TempLanguage);
            }
            catch { }

        }

        #endregion


        #region FourSystemSpoofing
        public static readonly DependencyProperty FourSystemSpoofingProperty = DependencyProperty.Register("FourSystemSpoofing", typeof(bool),
            typeof(SpoofingControl), new PropertyMetadata(true));

        public bool FourSystemSpoofing
        {
            get { return (bool)GetValue(FourSystemSpoofingProperty); }
            set { SetValue(FourSystemSpoofingProperty, value); }
        }

        public static readonly DependencyProperty FourSystemStatusProperty = DependencyProperty.Register("FourSystemStatus", typeof(Systems),
            typeof(SpoofingControl), new PropertyMetadata(new Systems(){Gps = true, Glonass = true, Beidou = true, Galileo = true}));

        public Systems FourSystemStatus
        {
            get { return (Systems)GetValue(FourSystemStatusProperty); }
            set { SetValue(FourSystemStatusProperty, value); }
        }

        #endregion


    }
}
