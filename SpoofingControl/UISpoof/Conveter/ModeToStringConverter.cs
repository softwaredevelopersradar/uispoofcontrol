﻿using System;
using System.Globalization;
using System.Windows.Data;


namespace SpoofingControl
{
    public class ModeToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string stringMode = "";
            if (value == null)
                return 0;

            if (parameter == null)
                return 0;

            switch (value)
            {
                case 0:
                    break;

                case 1:
                    break;

                case 2:
                    break;

                default:
                    break;
            }

            return stringMode;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
