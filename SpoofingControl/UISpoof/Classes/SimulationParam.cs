﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace UISpoof.Classes
{
    public class SimulationParam : INotifyPropertyChanged
    {

        
        private byte _range = 0;
        public byte Range
        {
            get { return _range; }
            set
            {
                if (_range != value)
                {
                    _range = value;
                    OnPropertyChanged();
                }
            }
        }

        private float _speed = 0;
        public float Speed
        {
            get { return _speed; }
            set
            {
                if (_speed != value)
                {
                    _speed = value;
                    OnPropertyChanged();
                }
            }
        }

        private short _angle;
        public short Angle
        {
            get { return _angle; }
            set
            {
                if (_angle != value)
                {
                    _angle = value;
                    OnPropertyChanged();
                    

                }
            }
        }

        private short _elevation;
        public short Elevation
        {
            get { return _elevation; }
            set
            {
                if (_elevation != value)
                {
                    _elevation = value;
                    OnPropertyChanged();
                }
            }
        }


        private Coordinate _coordinate = new Coordinate();
        public Coordinate Coordinate
        {
            get { return _coordinate; }
            set
            {
                if (_coordinate != value)
                {
                    _coordinate = value;
                    OnPropertyChanged();
                }
            }
        }

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        #endregion

    }
}
