﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace UISpoof.Classes
{
    public class Systems : INotifyPropertyChanged
    {
        private bool _gps;
        public bool Gps
        {
            get => _gps; 
            set
            {
                if (_gps == value) 
                    return;
                _gps = value;
                OnPropertyChanged();
            }
        }

        private bool _glonass;
        public bool Glonass
        {
            get => _glonass;
            set
            {
                if (_glonass == value)
                    return;
                _glonass = value;
                OnPropertyChanged();
            }
        }

        private bool _beidou;
        public bool Beidou
        {
            get => _beidou;
            set
            {
                if (_beidou == value)
                    return;
                _beidou = value;
                OnPropertyChanged();
            }
        }

        private bool _galileo;
        public bool Galileo
        {
            get => _galileo;
            set
            {
                if (_galileo == value)
                    return;
                _galileo = value;
                OnPropertyChanged();
            }
        }

        public Systems(){}
        public Systems(bool gps, bool glonass, bool beidou, bool galileo)
        {
            Gps = gps;
            Glonass = glonass;
            Beidou = beidou;
            Galileo = galileo;
        }



        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        #endregion
    }
}
