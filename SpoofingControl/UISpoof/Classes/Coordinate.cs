﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace UISpoof.Classes
{
    public class Coordinate : INotifyPropertyChanged
    {

        private float _latitude;
        public float Latitude
        {
            get { return _latitude; }
            set
            {
                if (_latitude != value)
                {
                    _latitude = value;
                }
            }
        }


        private float _longitude;
        public float Longitude
        {
            get { return _longitude; }
            set
            {
                if (_longitude != value)
                {
                    _longitude = value;
                }
            }
        }

        private float _altitude;
        public float Altitude
        {
            get { return _altitude; }
            set
            {
                if (_altitude != value)
                {
                    _altitude = value;
                }
            }
        }


        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        #endregion
    }
}
