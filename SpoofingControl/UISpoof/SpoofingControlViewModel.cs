﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using UISpoof.Classes;

namespace UISpoof
{
    public class SpoofingControlViewModel : INotifyPropertyChanged
    {

        public SpoofingControlViewModel()
        {
            
        }

        
        


        public float MinSpeed { get; private set; } = 0;

        public float MaxSpeed { get; private set; } = 40;



        public float MinElevation { get; private set; } = 0;

        public float MaxElevation { get; private set; } = 200;

        

       



        private bool[] _modeSpeed = new bool[] { false,true};
        public bool[] ModeSpeed
        {
            get { return _modeSpeed; }
        }
        public int SelectedMode
        {
            get { return Array.IndexOf(_modeSpeed, true); }
        }

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        #endregion

    }
}
