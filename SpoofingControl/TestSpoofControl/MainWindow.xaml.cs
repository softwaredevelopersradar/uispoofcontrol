﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UISpoof.Classes;

namespace TestSpoofControl
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        int step = 1;
       

        private SimulationParam _simulationParamInput = new SimulationParam();

        public SimulationParam SimulationParamInput
        {
            get 
            {
                return _simulationParamInput;
            }
            set
            {
                _simulationParamInput = value;
                OnPropertyChanged();
            }
        }



        private SimulationParam _simulationParamOutput = new SimulationParam();
        
        public SimulationParam SimulationParamOutput
        {
            get
            {
                return _simulationParamOutput;
            }
            set
            {
                _simulationParamOutput = value;
                OnPropertyChanged();
            }
        }

        

        private EStatus _mode = new EStatus();

        public EStatus Mode
        {
            get
            {
                return _mode;
            }
            set
            {
                _mode = value;
                OnPropertyChanged();
            }
        }



        private ELanguages _tempLanguageWnd = new ELanguages();

        public ELanguages TempLanguageWnd
        {
            get
            {
                return _tempLanguageWnd;
            }
            set
            {
                _tempLanguageWnd = value;
                OnPropertyChanged();
            }
        }
        public MainWindow()
        {
            InitializeComponent();
            Mode = 0;
            TempLanguageWnd = ELanguages.EN;
            DataContext = this;

        }

       
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Random rnd = new Random();
            step++;
            float lat = 0;
            float lon = 0;

            if (step == 5)
            {
                lat = 0;
                lon = 10;
            }
            else
            {
                lat = 53.0f + (float)(rnd.Next(0, 9) / 100f);
                lon = 27.0f + (float)(rnd.Next(0, 9) / 100f);
            }



                SimulationParamOutput = new SimulationParam()
            {
                Angle = (short)(rnd.Next(3, 235)),
                Speed = (float)(rnd.Next(0, 40)),
                Range = (byte)(rnd.Next(0, 40)),
                Elevation = (short)(rnd.Next(3, 235)),

                
                
                
                Coordinate = new Coordinate()
                {
                    Latitude = lat,
                    Longitude = lon
                },

                


            };

        }


        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        #endregion

       
        private void HandlerChangeInputParamEvent(object sender, SimulationParam e)
        {
            int f = 9;
        }

        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            Random rnd = new Random();

            SimulationParamInput = new SimulationParam()
            {
                Angle = (short)(rnd.Next(3, 235)),
                Speed = (float)(rnd.Next(0, 40)),
                Range = (byte)(rnd.Next(0, 40)),
                Elevation = (short)(rnd.Next(3, 235)),
                //Coordinate = new Coordinate()
                //{
                //    Latitude = 27.0f + (float)(rnd.Next(0, 9) / 100f),
                //    Longitude = 53.0f + (float)(rnd.Next(0, 9) / 100f)
                //},

                
            };

            
        }

        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            TempLanguageWnd = (TempLanguageWnd == ELanguages.EN) ? ELanguages.RU : ELanguages.EN;

           
        }

        private void Button4_Click(object sender, RoutedEventArgs e)
        {
            Mode = EStatus.Generation;
        }
    }
}
